//
//  Video.swift
//  FunBox
//
//  Created by Machintos on 3/19/17.
//  Copyright © 2017 Shariif Islam. All rights reserved.
//

import UIKit

//class Video: NSObject {
//
//    var id : String!
//    var thumbnailName : String!
//    var videoTitle : String!
//    var videoURL : String!
//    var videoSubtitle : String!
//}
struct Video : Codable {
    var id : Int?
    var videoTitle : String?
    var videoSubtitle : String?
    var videoURL : String?
    let comments : [Comments]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case videoTitle = "videoTitle"
        case videoSubtitle = "videoSubtitle"
        case videoURL = "videoURL"
        case comments = "Comments"
    }

}
class DSSCategory: NSObject {
    
    var categoryID : String!
    var categoryName : String!
}

class VideoContent: NSObject {
    
    var contentName : String!
    var contentImage : String!
    var contentURL : String!
}





