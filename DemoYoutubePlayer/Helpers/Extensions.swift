//
//  Extension.swift
//  DemoYoutubePlayer
//
//  Created by Mahesh Mahalik on 01/04/20.
//  Copyright © 2020 Mahesh Mahalik. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
      return  UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static func dssTintColor () -> UIColor {
        return UIColor.init(red: 200/255, green: 0/255, blue: 0/255, alpha: 1)
    }
    
    static func dssTitleColor () -> UIColor {
        return UIColor.init(red: 205/255, green: 205/255, blue: 205/255, alpha: 1)
    }
}


// UIView extention for adding constraints to all view
extension UIView {
    
    func addConstraintsWithFormats(format:String, views: UIView...)  {
        
        var viewsDictionary = [String:UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}

let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    func loadImageUsingUrlString(_ urlString: String) {
        
        imageUrlString = urlString
        let url = URL(string: urlString)
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, respones, error) in
            
            if error != nil {
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                let imageToCache = UIImage(data: data!)
                
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)
            })
            
        }).resume()
    }
}

extension AVAsset{
    var videoThumbnail:UIImage?{

        let assetImageGenerator = AVAssetImageGenerator(asset: self)
        assetImageGenerator.appliesPreferredTrackTransform = true

        var time = self.duration
        time.value = min(time.value, 2)

        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbNail = UIImage.init(cgImage: imageRef)


           // print("Video Thumbnail genertated successfuly".DubugSuccess())

            return thumbNail

        } catch {

          //  print("error getting thumbnail video".DubugError(),error.localizedDescription)
            return nil


        }

    }
}
extension Array {
    var data:Data? {
        get{
            do{
                return (try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted))
            }catch let e {
                print(e.localizedDescription)
                return nil
            }
        }
    }
    
}


