//
//  Animation.swift
//  DemoYoutubePlayer
//
//  Created by Mahesh Mahalik on 01/04/20.
//  Copyright © 2020 Mahesh Mahalik. All rights reserved.
//

import UIKit

class Animation  {
    
    static let sharedInstance = Animation()

    func animate(button: UIButton) {
        
        button.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {
                        button.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
    }
    
}
