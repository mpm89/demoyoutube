//
//  APIManager.swift
//  DemoYoutubePlayer
//
//  Created by Mahesh Mahalik on 01/04/20.
//  Copyright © 2020 Mahesh Mahalik. All rights reserved.

import UIKit
import SystemConfiguration

class APIManager: NSObject {
    
    static let shared = APIManager()
    
    open func parseAPIData( callback:@escaping ([Video], Bool) -> ()) {
        if Reachability.isConnectedToNetwork() == true{
        if let path = Bundle.main.path(forResource: "videos", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [String:Any] {
                    
                    if let resData = jsonResult["result"] as? [[String:Any]]{
                        let array_videoSong = try JSONDecoder().decode([Video].self, from: resData.data!)
                        callback(array_videoSong, true)
                    }
                        
                        
                    else
                    {
                        print(" - response data data is nill - ")
                    }
                }
            } catch {
                // handle error
            }
        }
        }else{
            callback([], false)
        }
    }
        
    
    
}
public class Reachability {

    // Check if internet connection is available
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }


       
}

