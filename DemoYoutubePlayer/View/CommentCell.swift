//
//  CommentCell.swift
//  DemoYoutubePlayer
//
//  Created by Mahesh Mahalik on 01/04/20.
//  Copyright © 2020 Mahesh Mahalik. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgViewUser.layer.cornerRadius = imgViewUser.frame.height / 2
        imgViewUser.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
